%% 

clear all;
clc;

%% Inputs
% Users, Companies
U = 100; C=5;
% Low & High Bound of consumption
lb = 10;  hb = 200;
% Parameters        
k = 1000; l = 100;

% Load Parameters a, d, cost, discount factor f
load('a');
load('d');
load('cost');
load('f');
% Probablity Vector of each User, with equal probabilities as initial point
prob = zeros(U,C); prob(:,:)=1/C;
emin = zeros(U,1);
for u = 1:U
    percent = 0.35 + (0.65-0.35)*rand;
    emin(u) = percent*d(u);
end

% Users Choices regarding the Energy Comapnies
Choices = [];

% Users Consumption
Consumptions = [];

% Companies Prices
Prices = [];

% Companies Reputation Score
RC = [];

% Serving Energy for each Company
Energies = [];


Average_Probabilities = [];

%% Reinforcement Learning & Game Theory FrameWork
% ml -> logic variable that shows the Convergence to the NE point
ml = false; 
% Iterations until the convergence
ite = 0;
% Learning Parameter for the Stochastic Learning Automata
b = 0.7;
while (ml==false)
    disp('Iteration : ', num2str(ite));
    ite = ite + 1;

    % Each Users chooses an Energy Company regarding his probability vector
    choices = zeros(U,1);
    for u = 1:U
        choices(u) = randsample(1:C,1,true,prob(u,:)); % selecetion by specific probability vector
    end
    Choices = [Choices choices];
    % Prices for the Companies
    prices = setprices(Prices,ite,C);

    % Users Consumptions
    consumptions = setconsumptions(Consumptions,d,ite,U,emin);
    conv=false; % conv  -> logic variable that shows the convergence of the two stage game 
    while(conv==false)
        % Customers' Stage
        flagu=0; % logic variable that shows if the users have converged to a stable point for their consumption
        for u = 1:U
            % Total Consumption of the Network
            Egrid = sum(consumptions);
            % Company that user u chose
            company = Choices(u,ite);
            % Price of that company
            price = prices(company);
            if(a(u)>(l*k/price))
                a(u) = l*k/(2*price) + (l*k/price - l*k/(2*price))*rand;
            end
            % User Allocates his Optimal Consumption            
            val = k/(a(u)*price)-1/l;
            opt = Egrid*val/(val+1);
            opt = min([opt d(u)]);
            % At least the user will choose his minimum consumption
            if(opt<emin(u))
                opt = emin(u);
            end
            % Achievalbe Utility
            opt_ut = ut(opt,Egrid,k,l,a(u),price);
            if(opt_ut<0)
                flaggg = 100
                opt = 0;
            end
            % User checks if the consumption that chose is the same with his consumption of the previous instance of the while
            % If yes the User have converged to a stable consumption
            if(abs(consumptions(u)-opt)<0.1)
                flagu = flagu+1;
            end
            consumptions(u)=opt;            
        end
        % Total Consumption
        Egrid = sum(consumptions);
        
        % Companies' Stage
        flagc = 0; % logic variable that shows if the companies have converged to a stable point for their price
        % serving energy for each company
        energies = zeros(C,1);
        for c = 1:C
            compa = 0;
            comp = 0;
            % Company c finds the Users that have choosen it 
            for u = 1:U
                if(Choices(u,ite)==c)
                    energies(c) = energies(c) + consumptions(u);
                    Ecomp = Egrid - consumptions(u);
                    compa = compa + Ecomp/a(u); comp = comp + Ecomp;
                end
            end
            % If the company have not been choosen from any user, its price will be zero 
            if(energies(c)==0)
                prices(c)=0;
                flagc = flagc + 1;
                continue;
            end
            % Comapny allocats its optimal price      
            opt = sqrt(k*l*cost(c)*compa/((1-f(c))*comp));
            % Company checks if the price that chose is the same with his price of the previous instance of the while
            if(abs(prices(c)-opt)<0.1)
                flagc = flagc + 1;
            end
            prices(c)=opt;
        end
        % If all the Companies converged to their prices and all Users converged to their consumptions the two stage game ends 
        if(flagu==U && flagc==C)
            Consumptions = [Consumptions consumptions];
            Prices = [Prices prices];
            Energies = [Energies energies];
            conv = true;
        end
           
    end
    
 % Update Reputation Score for each Company
    rc = zeros(C,1);
    total = 0;
    for i = 1:ite
        total = total+sum(Energies(:,i));
    end
    for c = 1:C
        peak = max(Energies(c,1:ite));
        eavg = mean(Energies(c,1:ite));
        par = peak / eavg;
        rc(c) = f(c)*ite*(1/par)*(sum(Energies(c,:))/total); 
    end
    RC = [RC rc];

    % Update Users Probabilities regarding the Stochasti Learning Automata FrameWork
    flagp=0;
    for u = 1:U
        company = Choices(u,ite);
        reward = RC(company,ite) / sum(RC(:,ite));
        for c = 1:C
            if(c==company)
                prob(u,c) = prob(u,c)+b*reward*(1-prob(u,c));
            else
                prob(u,c) = prob(u,c)-b*reward*prob(u,c);
            end
            % Check if the User has converged of what company will choose
            if(abs(prob(u,c)-1)<0.1)
                flagp = flagp+1;
            end
        end
    end
    % If all Users have converged for the company what they will choose the Stochastic Learning Automata FrameWork ends
    if(flagp==U)
        ml = true;
    end
    
    % Average_Probabilities
    temp_prob = zeros(C,1);
    for c = 1:C
        temp_prob(c) = sum(prob(:,c))/U;
    end
    Average_Probabilities = [Average_Probabilities temp_prob];
end





%% Figures


color = cell(1,6);
color{1,1} = [0, 0.4470, 0.7410];
color{1,2} = [0.8500, 0.3250, 0.0980];
color{1,3} = [0.9290, 0.6940, 0.1250];
color{1,4} = [0.4940, 0.1840, 0.5560];
color{1,5} = [0.4660, 0.6740, 0.1880];
color{1,6} = [0.6350, 0.0780, 0.1840];
%% Results
% test = 1;
% % RC-Score
figure(); 
axes('box','on');
grid on; 
hold on;

h = zeros(1,5);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:5
    col = color{1,j};
    h(j) = plot(1:ite,RC(j,1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Company 1', 'Company 2','Company 3','Company 4','Company 5');
hold off;
xlabel('Iterations');
ylabel('RC-Score');
%axis([M(1) M(18) 1 Tmeans(5,1)]) ;
set(gca,'FontWeight','bold');
% 
% % Average Probabilities
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,5);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:5
    col = color{1,j};
    h(j) = plot(1:ite,Average_Probabilities(j,1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Company 1', 'Company 2','Company 3','Company 4','Company 5');
hold off;
xlabel('Iterations');
ylabel('Average Probability among Users');
set(gca,'FontWeight','bold');
% 
% 
% % Number of Users
Users = zeros(C,ite);
for c = 1:C
    for t = 1:ite
        s = 0;
        for u = 1:U
            if(Choices(u,t)==c)
                s = s+1;
            end
        end
        Users(c,t)=s;      
    end
end
% 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,5);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:5
    col = color{1,j};
    h(j) = plot(1:ite,Users(j,1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Company 1', 'Company 2','Company 3','Company 4','Company 5');
hold off;
xlabel('Iterations');
ylabel('Number of Users');
set(gca,'FontWeight','bold');
% 
% % Consumption Convergence
% 
figure(); 
axes('box','on');
grid on; 
hold on;
users = [9 10 9 10 9 10];
h = zeros(1,6);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
colorstring = 'mrmrmr';
for j = 1:6
    if(mod(j,2)==0)
        colors = [0.6350, 0.0780, 0.1840];
    else
        colors = [0, 0.4470, 0.7410];
    end
    if(j<=2)
        user = users(j);
        temp(1:ite)=d(user);
        h(j) = plot(1:1:ite,temp(1:1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',colors);
    elseif(j<=4)
        user = users(j);
        h(j) = plot(1:1:ite,Consumptions(user,1:1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',colors);
    else
        user = users(j);
        temp(1:ite)=emin(user);
        h(j) = plot(1:1:ite,temp(1:1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',colors);
        
    end
end

legend('Max-Consumption-User9', 'Max-Consumption-User10','Optimal-Consumption-User9','Optimal-Consumption-User10','Min-Consumption-User9','Min-Consumption-User10');
hold off;
xlabel('Iterations');
ylabel('Energy [kwh]');
set(gca,'FontWeight','bold');
% % 
% % 
% % % Price Convergence
% % 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,5);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:5
    col = color{1,j};
    if(Prices(j,ite)~=0)
        h(j) = plot(1:ite,Prices(j,1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
    end
end
legend('Company 1', 'Company 3','Company 4','Company 5');
hold off;
xlabel('Iterations');
ylabel('Price');
set(gca,'FontWeight','bold');
% 
% % Average Utility
Utilities = [];
utilities = zeros(U,1);
for t = 1:ite
    for u = 1:U
        company = Choices(u,t);
        price = Prices(company,t);
        x = Consumptions(u,t);
        Egrid = sum(Consumptions(:,t));
        utilities(u) = ut(x,Egrid,k,l,a(u),price);
    end
    Utilities = [Utilities utilities];
end
Avg_Utilities = zeros(1,ite);
for t = 1:ite
    Avg_Utilities(t) = sum(Utilities(:,t))/U;
end
% 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,1);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:1
    col = color{1,j};
    h(j) = plot(1:ite,Avg_Utilities,strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Average Utility among Users');
hold off;
xlabel('Iterations');
ylabel('Utility');
set(gca,'FontWeight','bold');
% 
% % Average Welfare
Welfares = [];
welfares = zeros(C,1);
for t = 1:ite
    for c = 1:C
        price = Prices(c,t);
        welfares(c) = (1-f(c))*price*Energies(c,t) - Energies(c,t)*cost(c);
    end
    Welfares = [Welfares welfares];
end

Avg_Welfares = zeros(1,ite);
for t = 1:ite
    Avg_Welfares(t) = sum(Welfares(:,t))/C;
end

figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,1);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:1
    col = color{1,j};
    h(j) = plot(1:ite,Avg_Welfares,strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Average Welfare among Companies');
hold off;
xlabel('Iterations');
ylabel('Welfare');
set(gca,'FontWeight','bold');


% Total Consumption
Total_Consumption = zeros(1,ite);
for t = 1:ite
    Total_Consumption(t) = sum(Consumptions(:,t));
end
% 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,2);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:1
    col = color{1,j};
    h(j) = plot(1:ite,Total_Consumption,strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
temp(1:ite) = sum(d);
h(2) = plot(1:ite,temp,strcat('-',markers{2}),'LineWidth',2,'MarkerSize',2,'Color',color{1,2});
legend('Total Energy Consumption', 'Total Demand' );
hold off;
xlabel('Iterations');
ylabel('Energy [kwh]');
set(gca,'FontWeight','bold');
% 
% 
% % Relative Consumption
% rel_cons = zeros(U,ite);
% for t = 1:ite
%     for u = 1:U
%         cons = Consumptions(u,t);
%         rel_cons(u,t) = cons/(sum(Consumptions(:,t)) - cons);
%     end
% end
% 
% 
% figure(); 
% axes('box','on');
% grid on; 
% hold on;
% h = zeros(1,1);
% markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
% for j = 1:1
%     col = color{1,j};
%     h(j) = plot(1:ite,rel_cons(50,:),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
% end
% legend('Relative Consumption');
% hold off;
% xlabel('Iterations');
% ylabel('Relative Consumption');
% set(gca,'FontWeight','bold');
% 
% avg_rel_cons = zeros(1,ite);
% for t = 1:ite
%     avg_rel_cons(t) = mean(rel_cons(:,t));
% end
% 
% figure(); 
% axes('box','on');
% grid on; 
% hold on;
% h = zeros(1,1);
% markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
% for j = 1:1
%     col = color{1,j};
%     h(j) = plot(1:ite,avg_rel_cons(1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
% end
% legend(' Average Relative Consumption among Users');
% hold off;
% xlabel('Iterations');
% ylabel('Relative Consumption');
% set(gca,'FontWeight','bold');
% 
% 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,C);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:C
    col = color{1,j};
    h(j) = plot(1:ite,Welfares(j,1:ite),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
legend('Company 1', 'Company 2', 'Company 3', 'Company 4', 'Company5');
hold off;
xlabel('Iterations');
ylabel('Welfare');
set(gca,'FontWeight','bold');
% 
% 
% % Companies Characteristics
names = {'Company 1'; 'Company 2'; 'Company 3'; 'Company 4'; 'Company 5';};
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,C);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:C
    col = color{1,j};
    h(j)= line([j j],[0 cost(j)],'LineWidth', 5,'Color', color{1,j});
end
%legend('Company 1', 'Company 2', 'Company 3', 'Company 4', 'Company5');
%line([0 0],[0 0],'LineWidth', 5,'Color', color{1,j});
%line([6 6],[0 0],'LineWidth', 5,'Color', color{1,j});

hold off;
xlabel('Companies');
ylabel('Costs');
set(gca,'xtick',[1:5],'xticklabel',names);
set(gca,'FontWeight','bold');
% 
figure(); 
axes('box','on');
grid on; 
hold on;
h = zeros(1,C);
markers = {'o','*','.','x','s','d','^','v','>','<','p','h'};
for j = 1:C
    col = color{1,j};
    SP = f(j);
    h(j)= line([j j],[0 f(j)],'LineWidth', 5,'Color', color{1,j});
    %h(j) = plot(j,cost(j),strcat('-',markers{j}),'LineWidth',2,'MarkerSize',2,'Color',col);
end
%legend('Company 1', 'Company 2', 'Company 3', 'Company 4', 'Company5');
hold off;
xlabel('Companies');
ylabel('Discounts');
set(gca,'xtick',[1:5],'xticklabel',names);
set(gca,'FontWeight','bold');



% B - Parameter Results
% bt_avg_utility = zeros(1,5);
% for i = 1:5
%     bt_avg_utility(i) = mean(b_Avg_Utility(i,:));
% end
% 
% bt_avg_welfare = zeros(1,5);
% for i = 1:5
%     bt_avg_welfare(i) = mean(b_Avg_Welfare(i,:));
% end
% 
% bt_iterations = zeros(1,5);
% for i = 1:5
%     bt_iterations(i) = mean(b_Iterations(i,:));
% end
% 
% bt_total = zeros(1,5);
% for i = 1:5
%     bt_total(i) = mean(b_Total_Consumption(i,:));
% end
% 
% b_param = [0.9 0.8 0.7 0.5 0.3];
% 
% figure(); 
% axes('box','on'); 
% hold on;  
% yyaxis left 
% plot(b_param,bt_iterations,'o-','LineWidth',1.5,'MarkerSize',6); 
% axis([0.3 0.9 bt_iterations(1) bt_iterations(5)]) ; 
% ylabel('Iterations'); 
% yyaxis right 
% plot(b_param,bt_avg_utility,'d--','LineWidth',1.5,'MarkerSize',6); 
% axis([0.3 0.9 bt_avg_utility(1) bt_avg_utility(5)]) ; 
% ylabel('Average Utility among Users'); 
% grid on; 
% lgd =legend('Convergence vs b','Average Utility vs b','east'); 
% lgd.FontSize = 11; xlabel('Learning Parameter b'); 
% set(gca,'XTick',(0:0.1:1.1)); 
% hold off; 
% set(gca,'FontWeight','bold');
% 
% figure(); 
% axes('box','on'); 
% hold on;  
% yyaxis left 
% plot(b_param,bt_iterations,'o-','LineWidth',1.5,'MarkerSize',6); 
% axis([0.3 0.9 bt_iterations(1) bt_iterations(5)]) ; 
% ylabel('Iterations'); 
% yyaxis right 
% plot(b_param,bt_avg_welfare,'d--','LineWidth',1.5,'MarkerSize',6); 
% axis([0.3 0.9 bt_avg_welfare(1) bt_avg_welfare(5)]) ; 
% ylabel('Average Welfare among Companies'); 
% grid on; 
% lgd =legend('Convergence vs b','Average Welfare vs b','east');
% lgd.FontSize = 11; xlabel('Learning Parameter b'); 
% set(gca,'XTick',(0:0.1:1.1)); 
% hold off; 
% set(gca,'FontWeight','bold');
% 
% 
