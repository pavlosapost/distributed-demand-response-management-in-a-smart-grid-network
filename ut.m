function [utility] = ut(x,Egrid,k,l,a,price)
tp = x/(Egrid-x);
utility = k*log(1+l*tp) - a*price*tp;
end

