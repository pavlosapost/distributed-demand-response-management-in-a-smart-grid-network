function [prices] = setprices(Prices,ite,C)
prices = zeros(C,1);
if(ite==1)
    for c = 1:C
        %prices(c) = rand + 0.01; % first price of each company will be random
         prices(c) = 1; % or all companies have the same initial price
    end
else
	% Companies have the prices of the previous iteration
    prices = Prices(1:C,ite-1);
end

end

