function [w] = welfare(c,price,Egrid,Choices,consumptions,a,f,cost,ite,k,l,U)

acc = 0;
for u = 1:U
    if(Choices(u,ite)==c)
        Eu = Egrid - consumptions(u);
        val = k/(a(u)*price)-1/l;
        acc = acc + Eu*val;
    end
end
w = (1-f)*price*acc - cost*acc;
end

