U =300;     T=100;     C=5;                                       
lb = 10; hb = 200;           % Low & High Bound of consumption
d = zeros(U,T);                 % Demand vector
a = zeros(U,T);                 % Parameter vector 
choice = zeros(U,T);      % Choice Vector
Pr = zeros(T,U,C);           % Probability Vector
% Probabibility for first time slot
for u = 1:U
    for co = 1:C
        Pr(1,u,co)  = 1/C; 
    end
end


% Demand Vector Random Initiallization
for u = 1:U
    for t = 1:T
        d(u,t) = lb+(hb-lb)*rand;
        d(u,t) = round(d(u,t)*10000)/10000; % 4 digits
    end
end


f = zeros(C,T);
c = zeros(C,T);

 f1 = 0.08;    f2 = 0.07;     f3= 0.03;    f4 = 0.06;    f5 = 0.05;
 c1 = 0.7;   c2 = 0.5;     c3 = 0.2;   c4 = 0.4;     c5 = 0.5;
% % f2 = f1;  f3 = f1;  f4 = f1;  f5 = f1;
% % c2 = c1; c3 = c1; c4 = c1; c5 = c1;
% 
T=1;
c(1,1:T) = c1;  c(2,1:T) = c2;  c(3,1:T) = c3;  c(4,1:T) = c4;  c(5,1:T) = c5;
f(1,1:T) = f1;  f(2,1:T) = f2;  f(3,1:T) = f3;  f(4,1:T) = f4;  f(5,1:T) = f5;

% last 6-timeslots we have large amount of demands 
% f1 = 0.12;    f2 = 0.1;     f3= 0.13;    f4 = 0.15;    f5 = 0.11;
% f(1,18:T) = f1;  f(2,18:T) = f2;  f(3,18:T) = f3;  f(4,18:T) = f4;  f(5,18:T) = f5;


% 

% for co = 1:C
%     valuef = 0.01 + 0.1*rand;
%     valuec = 0.06+ 0.08*rand;
%     for t = 1:T
%         f(co,t) = valuef;
%         c(co,t) = valuec;
%     end
% end


% for t = 1:T
%         for co = 1:C
%             f(co,t) = 0.0 + (0.9-0.0)*rand; % discount rate in range 0-90 %
%             f(co,t) = round(f(co,t)*10000)/10000; % 4 digits
%             c(co,t) = 0.06 + (0.1-0.06)*rand; % cost rate in range 0.06-0.1 cents per kwh
%             c(co,t) = round(c(co,t)*10000)/10000; % 4 digits
%         end
% end

dlmwrite('demands',d,'\t');
dlmwrite('discounts',f,'\t');
dlmwrite('cost',c,'\t');



