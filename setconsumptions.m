function [consumptions] = setconsumptions(Consumptions,d,ite,U,emin)

if(ite==1)
	% for the first iteration the users choose to consume their minimum consumption
    consumptions=emin;
else
	% the users start with the consumption of the previous iteration
    consumptions = Consumptions(1:U,ite-1);
end



end

